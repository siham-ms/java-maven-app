def buildJar() {
    echo "building the application..."
    sh 'mvn clean package'
} 

def SonarqubeTest() {
    echo 'Reviewing the code with sonarqube'
    }



def buildImage() {
    echo "building the docker image..."
    withCredentials([usernamePassword(credentialsId: 'docker-hub-credentials', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
        sh 'docker build -t sihamms/siham-repo:jma-ec2 .'
        sh "echo $PASS | docker login -u $USER --password-stdin"
        sh 'docker push sihamms/siham-repo:jma-ec2'
    }
} 

def deployApp() {
    echo 'deploying the application to the ec2 server...'
    def dockercomposecmd = 'docker compose -f docker-compose.yaml up --detach'
    sshagent(['ec2-key']) {
        sh "scp -o StrictHostKeyChecking=no docker-compose.yaml ec2-user@51.44.15.158:/home/ec2-user"
        sh "ssh -o StrictHostKeyChecking=no ec2-user@51.44.15.158 ${dockercomposecmd}"
}
} 


return this
